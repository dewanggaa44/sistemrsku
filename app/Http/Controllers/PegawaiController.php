<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PegawaiController extends Controller
{
    public function index()
    {
        $pegawai = DB::table('master_pegawai')->get();
        return view('index', ['pegawai' => $pegawai]);
    }

    public function tambah()
    {
        return view('tambah');
    }

    public function store(Request $request)
    {
        DB::table('master_pegawai')->insert([
            'nip' => $request->nip,
            'nama_pegawai' => $request->nama_pegawai,
            'alamat' => $request->alamat,
            'jabatan' => $request->jabatan,
            'jenis_kelamin' => $request->jenis_kelamin,
            'poli' => $request->poli
        ]);
        return redirect('/pegawai');
    }

    public function edit($nip)
    {
        $pegawai = DB::table('master_pegawai')->where('nip', $nip)->get();
        return view('edit', ['pegawai' => $pegawai]);
    }

    public function update(Request $request)
    {
        DB::table('master_pegawai')->where('nip', $request->nip)->update([
            'nama_pegawai' => $request->nama_pegawai,
            'alamat' => $request->alamat,
            'jabatan' => $request->jabatan,
            'jenis_kelamin' => $request->jenis_kelamin,
            'poli' => $request->poli
        ]);
        return redirect('/pegawai');
    }

    public function hapus($nip)
    {
        DB::table('master_pegawai')->where('nip', $nip)->delete();
        return redirect('/pegawai');
    }
}
