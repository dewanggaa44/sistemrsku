<!DOCTYPE html>
<html>

<head>
    <title>Sistem Informasi RS</title>
</head>

<body>

    <h3>Data Pegawai</h3>

    <a href="/pegawai"> Kembali</a>

    <br />
    <br />

    <form action="/pegawai/store" method="post">
        {{ csrf_field() }}
        NIP <input type="number" name="nip" required="required"> <br />
        Nama <input type="text" name="nama_pegawai" required="required"> <br />
        Alamat <textarea name="alamat" required="required"></textarea> <br />
        Jabatan <input type="text" name="jabatan" required="required"> <br />
        Jenis kelamin <input type="text" name="jenis_kelamin" required="required"> <br />
        Poli <input type="number" name="poli" required="required"> <br />
        <input type="submit" value="Simpan Data">
    </form>

</body>

</html>