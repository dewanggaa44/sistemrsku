<!DOCTYPE html>
<html>

<head>
    <title>Sistem Informasi RS</title>
</head>

<body>
    <h3>Data Pegawai</h3>

    <a href="/pegawai/tambah"> + Tambah Pegawai Baru</a>

    <br />
    <br />

    <table border="1">
        <tr>
            <th>NIP</th>
            <th>Nama</th>
            <th>Alamat</th>
            <th>Jabatan</th>
            <th>Jenis Kelamin</th>
            <th>Poli</th>
        </tr>
        @foreach($pegawai as $p)
        <tr>
            <td>{{ $p->nip }}</td>
            <td>{{ $p->nama_pegawai }}</td>
            <td>{{ $p->alamat }}</td>
            <td>{{ $p->jabatan }}</td>
            <td>{{ $p->jenis_kelamin }}</td>
            <td>{{ $p->poli }}</td>
            <td>
                <a href="/pegawai/edit/{{ $p->nip }}">Edit</a>
                |
                <a href="/pegawai/hapus/{{ $p->nip }}">Hapus</a>
            </td>
        </tr>
        @endforeach
    </table>


</body>

</html>