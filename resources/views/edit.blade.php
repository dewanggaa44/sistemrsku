<!DOCTYPE html>
<html>

<head>
    <title>Sistem Informasi RS</title>
</head>

<body>

    <h3>Edit Pegawai</h3>

    <a href="/pegawai"> Kembali</a>

    <br />
    <br />

    @foreach($pegawai as $p)
    <form action="/pegawai/update" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="nip" value="{{ $p->nip }}"> <br />
        Nama <input type="text" required="required" name="nama_pegawai" value="{{ $p->nama_pegawai }}"> <br />
        Alamat <textarea required="required" name="alamat">{{ $p->alamat }}</textarea> <br />
        Jabatan <input type="text" required="required" name="jabatan" value="{{ $p->jabatan }}"> <br />
        Jenis Kelamin <input type="text" required="required" name="jenis_kelamin" value="{{ $p->jenis_kelamin }}"> <br />
        Poli <input type="number" required="required" name="poli" value="{{ $p->poli }}"> <br />
        <input type="submit" value="Simpan Data">
    </form>
    @endforeach


</body>

</html>